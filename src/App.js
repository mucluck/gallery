import React, { Component, Fragment } from "react";
import ErrorBoundary from "./components/ErrorBoundary/ErrorBoundary"
import Header from "./components/Header/Header"
import Body from "./components/Body/Body";
import Footer from "./components/Footer/Footer"
import { BrowserRouter as Router } from 'react-router-dom'
import "./configurations/styles/main.scss"
import { isMobile } from "react-device-detect";

import Main from "./components/Pages/Main/Main";
import URL from "./components/Pages/Url/Url";
import JSON from "./components/Pages/Json/Json";
import FILE from "./components/Pages/File/File";

const GalleryIcon = <svg enable-background="new 0 0 30 30" height="30px" version="1.1" viewBox="0 0 30 30" width="30px" xmlns="http://www.w3.org/2000/svg"><path d="M24.3,11.8c-0.1-0.2-0.4-0.3-0.6-0.3c-0.3,0-0.5,0.1-0.6,0.3l-3.7,5.1l-2.1-2.5c-0.1-0.2-0.4-0.3-0.6-0.3  c-0.2,0-0.4,0.1-0.6,0.3L9,23.2c-0.3,0.3-0.2,0.8,0.1,1.1c0.1,0.1,0.3,0.2,0.5,0.2c0.2,0,0.4-0.1,0.6-0.3l6.5-8.1l2.2,2.6  c0.1,0.2,0.4,0.3,0.6,0.3c0.2,0,0.4-0.1,0.6-0.3l3.6-5l2.4,3.5c0.2,0.3,0.7,0.4,1,0.2c0.3-0.2,0.4-0.7,0.2-1L24.3,11.8z M5.5,5.2H23  c0.4,0,0.8-0.3,0.8-0.8S23.4,3.7,23,3.7H5.5c-3.4,0-5.4,2-5.4,5.4v10.3c0,0.4,0.3,0.8,0.8,0.8s0.8-0.3,0.8-0.8V9.1  C1.6,6.5,2.9,5.2,5.5,5.2z M26.1,5.6H6c-2.1,0-3.8,1.7-3.8,3.8v13.2c0,2.1,1.7,3.8,3.8,3.8h20.1c2.1,0,3.8-1.7,3.8-3.8V9.4  C29.9,7.3,28.2,5.6,26.1,5.6z M28.4,22.6c0,1.2-1,2.3-2.3,2.3H6c-1.2,0-2.3-1-2.3-2.3V9.4c0-1.2,1-2.3,2.3-2.3h20.1  c1.2,0,2.3,1,2.3,2.3V22.6z M12.7,13.5c0-1.6-1.3-3-3-3c-1.6,0-3,1.3-3,3c0,1.6,1.3,3,3,3C11.4,16.5,12.7,15.1,12.7,13.5z M8.3,13.5  c0-0.8,0.7-1.5,1.5-1.5c0.8,0,1.5,0.7,1.5,1.5c0,0.8-0.7,1.5-1.5,1.5C9,15,8.3,14.3,8.3,13.5z" /></svg>;

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pages: [
        { path: "/", component: Main, name: "Image Gallery", icon: GalleryIcon },
        { path: "/url", component: URL, name: "URL" },
        { path: "/json", component: JSON, name: "JSON" },
        /* { path: "/file", component: FILE, name: "FILE" } */
      ]
    }
  }

  render() {
    return (
      <ErrorBoundary>
        <Router>
          <Fragment>
            <Header
              name={"Image gallery"}
              links={this.state.pages}
              isMobile={isMobile}
            />
            <Body
              pages={this.state.pages}
              isMobile={isMobile}
            />
            <Footer
              links={this.state.pages}
              isMobile={isMobile}
            />
          </Fragment>
        </Router>
      </ErrorBoundary>
    );
  }
}
