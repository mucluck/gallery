import React, { Component, Fragment } from "react";
import uuid from "uuid";
import Navigation from "../Navigation/Navigation";
import cx from "classnames";

export default class Footer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: this.props.id || uuid(),
        }
    }

    render() {
        return (
            <Fragment>
                <footer
                    id={this.state.id}
                >
                    <div className={"container"}>
                        {this.props.isMobile && <Navigation
                            links={this.props.links}
                        />}
                    </div>
                </footer>
            </Fragment>
        );
    }
}