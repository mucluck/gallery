import React, { Component } from 'react';
import uuid from 'uuid';
import cx from "classnames";

export default class DataInput extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: this.props.id || uuid(),
            name: this.props.name || "",
            type: this.props.type || "text",
            value: this.props.value || "",
            className: this.props.className || "",
            items: this.props.items || [],
            label: this.props.label || "Choose a file",
            placeholder: this.props.placeholder || "",
            disabled: this.props.disabled || false,
            autoComplete: this.props.autoComplete || "off",
            title: this.props.title || "",
            changed: false
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState(nextProps)
    }

    handleChange(event) {
        this.setState({
            value: event.currentTarget.value,
            changed: true
        }, () => this.props.handler && this.props.handler(this));
    }

    get fieldValidator() {
        switch (this.state.name) {
            case "url":
                return /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/.test(this.state.value);
            case "json":
                try {
                    JSON.parse(this.state.value);
                    return true;
                } catch (event) {
                    return false;
                }
            default:
                return true
        }
    }

    get switchType() {
        switch (this.state.type) {
            case "file":
                return <fieldset
                    className={"text-center"}
                >
                    <label
                        htmlFor={this.state.id}
                    >
                        <span
                            className={"btn btn-sm btn-success"}
                        >
                            {this.state.label}
                        </span>
                    </label>
                    <input
                        id={this.state.id}
                        className={`${this.state.className} d-none`}
                        name={this.state.name}
                        type={this.state.type}
                        value={this.state.value}
                        onChange={this.handleChange.bind(this)}
                    />
                </fieldset>
            case "select":
                return this.state.items.length ? <select
                    id={this.state.id}
                    className={this.state.className}
                    onChange={this.handleChange.bind(this)}
                >
                    {this.state.items.map((item, idx) =>
                        <option
                            key={uuid()}
                            selected={item.value === this.state.value || item.value === this.props.selected || !idx}
                            value={item.value}
                        >
                            {item.name}
                        </option>
                    )}
                </select> :
                    null;
            case "textarea":
                return <textarea
                    id={this.state.id}
                    className={cx("data-input data-input_textarea", this.state.className)}
                    name={this.state.name}
                    value={this.state.value}
                    onChange={this.handleChange.bind(this)}
                ></textarea>
            default:
                return <input
                    id={this.state.id}
                    className={cx("data-input", this.state.className, { "data-input_invalid": !this.fieldValidator && this.state.changed }, { "data-input_valid": this.fieldValidator && this.state.changed })}
                    name={this.state.name}
                    type={this.state.type}
                    value={this.state.value}
                    onChange={this.handleChange.bind(this)}
                    placeholder={this.state.placeholder}
                    disabled={this.state.disabled}
                    autoComplete={this.state.autoComplete}
                    title={this.state.title}
                />
        }
    }

    render() {
        return this.switchType;
    }
}