import React, { Component } from 'react';
import axios from 'axios';
import uuid from 'uuid';

export default class Form extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: this.props.id || uuid(),
        }
    }

    handleSubmit(event) {
        event.preventDefault();
        event.stopPropagation();

        this.props.handler && this.props.handler(this);
    }

    render() {
        return this.props.children ? <form
            id={this.state.id}
            onSubmit={this.handleSubmit.bind(this)}
        >
            {this.props.children}
        </form> : null;
    }
}