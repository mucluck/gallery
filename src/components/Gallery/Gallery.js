import React, { Component } from "react";
import uuid from "uuid";
import axios from "axios";
import cx from "classnames";

class Image extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: this.props.id || uuid(),
            url: this.props.url || "",
            width: this.props.width || 0,
            height: this.props.height || 0,
            loaded: this.props.loaded
        }
    }

    loadImage() {
        const proxy = "https://cors-anywhere.herokuapp.com/";

        axios(
            {
                method: "GET",
                url: `${proxy}${this.state.url}`,
                responseType: "arraybuffer",
                headers: {
                    'Access-Control-Allow-Origin': '*',
                },
            })
            .then((response) => {
                this.setState({
                    url: new Buffer(response.data, "binary").toString("base64"),
                    loaded: true
                });
            });
    }

    render() {
        return (
            !!this.state.url && <div
                id={this.state.id}
                className={this.props.className}
                style={{
                    "flexBasis": this.state.width,
                    "minHeight": this.state.height
                }}
            >
                {this.state.loaded ? <img src={this.state.url} /> : <div className="loader"></div>}
            </div>
        )
    }
}

export default class Gallery extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: this.props.id || uuid(),
            inputType: "text",
            images: this.props.images,
            calculatedImages: [],
            galleryWidth: 0,
            referenceMaxHeight: this.props.referenceMaxHeight || 200,
            referenceMinHeight: this.props.referenceMinHeight || 100,
            loading: this.props.loading || false,
            initialCols: this.props.cols || 4,
            cols: window.outerWidth > 768 ? (this.props.cols || 4) : 2,
            recalculated: false,
            resize: true,
        }
    }

    componentDidMount() {
        window.addEventListener("resize", () => {
            this.setState({
                galleryWidth: this.gallery ? this.gallery.offsetWidth : 0,
                cols: window.outerWidth > 768 ? this.state.initialCols : 2,
                resize: this.gallery && (this.gallery.offsetWidth !== this.state.galleryWidth),
            }, () => this.state.resize && this.calculateImageSize());
        });
    }

    componentWillUnmount() {
        window.removeEventListener("resize", () => { });
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            images: nextProps.images,
            galleryWidth: this.gallery ? this.gallery.offsetWidth : 0,
            resize: false,
        }, this.calculateImageSize);
    }

    caclulateImageRowSize(images = [], colStart = 0, colEnd = this.state.cols, cols = this.state.cols) {
        let width = images.slice(colStart, colEnd).reduce((width, image) => width += image.calculatedWidth, 0);

        images.splice(colStart, cols, ...images.slice(colStart, colEnd).reduce((images, image) => {
            let height = (this.state.referenceMaxHeight * this.state.galleryWidth) / width;
            images.push(Object.assign({}, image, { calculatedWidth: (height * image.width) / image.height, calculatedHeight: height }));

            return images;
        }, []));

        return images;
    }

    calculateImageRows(colStart = 0, colEnd = this.state.cols, images = this.state.images, cols = this.state.cols) {
        if (images && this.state.images.length) {
            let width = images.slice(colStart, colEnd).reduce((width, image) => width += image.calculatedWidth, 0);

            if (width < this.state.galleryWidth) {
                cols += 1;
                colEnd = colStart + cols;
                images = this.caclulateImageRowSize(images, colStart, colEnd, cols);
            } else if (width > this.state.galleryWidth) {
                colEnd = colStart + cols;
                images = this.caclulateImageRowSize(images, colStart, colEnd, cols);
            }

            colStart = colEnd;
            colEnd = colStart + cols;

            images.slice(colStart, colEnd).length >= cols ?
                this.calculateImageRows(colStart, colEnd, images) :
                this.setState({
                    images,
                });
        }
    }

    calculateImageSize() {
        this.state.images && this.state.images.length && this.setState({
            images: this.state.images.reduce((images, image) => {
                images.push(Object.assign({}, image, {
                    calculatedWidth: (this.state.referenceMaxHeight * image.width) / image.height,
                    calculatedHeight: this.state.referenceMaxHeight,
                }));

                return images;
            }, [])
        }, this.calculateImageRows);
    }

    inputTypeHandler(event) {
        this.setState({
            inputType: event.state.value,
        });
    }

    render() {
        return <div
            id={this.state.id}
            ref={gallery => this.gallery = gallery}
        >
            {this.state.images && <div
                className={cx("gallery", { "gallery_loading": !this.state.images.length })}
            >
                {this.state.images.length ? this.state.images.map((image, idx) => <Image
                    idx={idx}
                    id={image.id}
                    className={"gallery__item"}
                    key={uuid()}
                    url={image.url}
                    width={image.calculatedWidth}
                    height={image.calculatedHeight}
                    loaded={image.loaded}
                />) : <div className="loader"></div>}
            </div>}
        </div>
    }
}