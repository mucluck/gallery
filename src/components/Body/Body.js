import React, { Component, Fragment } from 'react';
import uuid from 'uuid';
import { Route } from 'react-router-dom'

export default class Body extends Component {
    render() {
        return this.props.pages.map((page, idx) => <Route key={uuid()} exact={!idx} path={page.path} component={page.component} />);
    }
}