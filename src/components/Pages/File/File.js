import React, { Component } from 'react';
import Form from "../../Form";
import TextInput from "../../Form/DataInput/DataInput";

export default class File extends Component {
    render() {
        return (
            <div className="container text-center">
                <h1 className="display-4 font-weight-normal">Download from FILE</h1>
                <p className="lead font-weight-normal">Try responsive image gallery with different uploading ways</p>
                <Form>
                    <fieldset>
                        <TextInput
                            className={"form-control"}
                            type={"file"}
                            name={"file"}
                        />
                    </fieldset>
                    <fieldset>
                        <TextInput
                            className={"btn btn-primary"}
                            type={"submit"}
                            name={"url"}
                            value={"Download"}
                        />
                    </fieldset>
                </Form>
            </div>
        );
    }
}