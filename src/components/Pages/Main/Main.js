import React, { Component } from 'react';
import { Link } from 'react-router-dom'

export default class Main extends Component {
    render() {
        return (
            <main>
                <div className="container text-center">
                    <h1 className="">Image gallery</h1>
                    <p className="">Try a responsive image gallery with different uploading ways</p>
                    <Link
                        to="/url"
                        className="btn"
                    >
                        Get started
                </Link>
                </div>
            </main>
        );
    }
}