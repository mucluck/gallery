import React, { Component } from 'react';
import Form from "../../Form";
import DataInput from "../../Form/DataInput/DataInput";
import Gallery from "../../Gallery/Gallery";
import axios from "axios";
import uuid from "uuid";

export default class Url extends Component {
    constructor(props) {
        super(props);

        this.state = {
            url: "",
            valid: false,
            error: "",
            gallery: false,
            images: null,
            request: true,
            loadedImages: [],
        }
    }

    getData() {
        const proxy = "https://cors-anywhere.herokuapp.com/";

        (this.state.url !== this.input.state.value) && this.input.fieldValidator && this.setState({
            url: this.input.state.value,
            images: [],
            error: "",
        }, () => {
            axios(
                {
                    method: "GET",
                    url: `${proxy}${this.state.url}`,
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                        'Content-Type': 'application/json'
                    },
                }).then(response => {
                    this.setState({
                        images: JSON.parse(`{${response.data}}`).galleryImages,
                    }, async () => {
                        await this.state.images.forEach(async (image, idx) => {
                            let images = this.state.images;
                            let source = await axios(
                                {
                                    method: "GET",
                                    url: `${proxy}${image.url}`,
                                    responseType: "arraybuffer",
                                    headers: {
                                        'Access-Control-Allow-Origin': '*',
                                    },
                                });

                            images[idx] = {
                                width: image.width,
                                height: image.height,
                                url: `data:${source.headers["content-type"]};base64,${new Buffer(source.data, "binary").toString("base64")}`,
                                loaded: true,
                                id: uuid()
                            };

                            this.setState({
                                images
                            });
                        });
                    });
                }).catch(error => {
                    this.setState({
                        error: error,
                        images: null
                    });
                });
        });
    }

    render() {
        return (
            <main>
                <div
                    className="container text-center"
                >
                    <div>
                        <h1>Download from URL</h1>
                        <p>Enter file location</p>
                        <Form
                            handler={this.getData.bind(this)}
                        >
                            <fieldset
                                className="mb-3"
                            >
                                <DataInput
                                    ref={input => this.input = input}
                                    className={"form-control"}
                                    type={"url"}
                                    name={"url"}
                                    placeholder={"URL"}
                                />
                            </fieldset>
                            <fieldset
                                className="mb-3"
                            >
                                <DataInput
                                    className={"btn"}
                                    type={"submit"}
                                    name={"url"}
                                    value={"Download"}
                                    title={"Please, enter file location or choose another way"}
                                />
                            </fieldset>
                        </Form>
                        {!!this.state.error && <div className="alert alert_danger">{this.state.error.message}</div>}
                        <Gallery
                            referenceHeight={200}
                            images={this.state.images}
                        />
                    </div>
                </div>
            </main>
        );
    }
}