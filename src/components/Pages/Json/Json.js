import React, { Component } from 'react';
import Form from "../../Form";
import DataInput from "../../Form/DataInput/DataInput";
import Gallery from "../../Gallery/Gallery";
import ClassNames from "classnames";
import axios from "axios";
import uuid from "uuid";

export default class Json extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: this.props.value || "",
            valid: false,
            images: null
        };
    }

    setValue(event) {
        this.setState({
            value: event.state.value,
            valid: event.fieldValidator
        });
    }

    async getData() {
        const proxy = "https://cors-anywhere.herokuapp.com/";
        this.setState({ images: JSON.parse(this.state.value).galleryImages || [] }, () => this.state.images && this.state.images.forEach(async (image, idx) => {
            let images = this.state.images;
            let source = await axios(
                {
                    method: "GET",
                    url: `${proxy}${image.url}`,
                    responseType: "arraybuffer",
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                    },
                });

            images[idx] = {
                width: image.width,
                height: image.height,
                url: `data:${source.headers["content-type"]};base64,${new Buffer(source.data, "binary").toString("base64")}`,
                loaded: true,
                id: uuid()
            };

            this.setState({
                images
            });
        }));
    }

    render() {
        return (
            <main>
                <div className="container text-center">
                    <h1>Download from JSON</h1>
                    <p>Enter JSON</p>
                    <Form
                        handler={this.getData.bind(this)}
                    >
                        <fieldset>
                            <DataInput
                                type={"textarea"}
                                name={"json"}
                                handler={this.setValue.bind(this)}
                            />
                        </fieldset>
                        <fieldset
                            className="text-center"
                        >
                            <DataInput
                                className={ClassNames(
                                    { "disabled": !this.state.value }
                                )}
                                type={"submit"}
                                name={"url"}
                                value={"Download"}
                                disabled={!this.state.valid}
                            />
                        </fieldset>
                    </Form>
                    {!!this.state.error && <div className="alert alert_danger">{this.state.error.message}</div>}
                    <Gallery
                        referenceHeight={200}
                        images={this.state.images}
                    />
                </div>
            </main>
        );
    }
}