import React, { Component, Fragment } from "react";
import uuid from "uuid";
import Navigation from "../Navigation/Navigation";

export default class Header extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: this.props.id || uuid(),
        }
    }

    render() {
        return (
            <header
                id={this.state.id}
            >
                <div className="container">
                    {!this.props.isMobile && <Navigation
                        links={this.props.links}
                    />}
                </div>
            </header>
        );
    }
}
