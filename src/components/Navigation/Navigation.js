import React, { Component, Fragment } from 'react';
import uuid from 'uuid';
import { NavLink } from 'react-router-dom'

export default class Navigation extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: this.props.id || uuid(),
            links: this.props.links || []
        }
    }

    render() {
        return (
            <nav className={"navigation"}>
                {this.state.links.map(link =>
                    <NavLink
                        exact
                        key={uuid()}
                        className="navigation__item"
                        to={link.path}
                        activeClassName="navigation__item_active"
                    >
                        {link.icon || link.name}
                    </NavLink>
                )}
            </nav>
        );
    }
}
