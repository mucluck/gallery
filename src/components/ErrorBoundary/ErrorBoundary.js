import React, {Component} from "react";

export default class ErrorBoundary extends Component {
    constructor(props) {
        super(props);

        this.state = {
            hasError: false,
        }
    }

    componentDidCatch(error, info) {
        console.log(error, info);
        this.setState({
            hasError: true,
        });
    }

    render() {
        return this.state.hasError ? <div><p>Ошибка</p></div> : this.props.children;
    }
}